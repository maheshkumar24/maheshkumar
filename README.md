# Functional Requirements
Below are listed the functional requirements that the solution should support. Wherever in doubt, make reasonable assumptions and move forward but please do document those assumptions as well so that we know why you made them.

Let us imagine we are talking about a popular restaurant such as Paradise with its own restaurant management system. If we expand the various actors and sub systems involved, we could break it down as below
- A food menu which lists various sections (starters / main course / desserts) having items inside each.
- A price chart associated with every item on the menu.
- The customers who visit the restaurant.
- Hosts and hostesses who allot customers tables or queue them up if all tables are in use.
- Bus boys who serve water, clean tables, lay out fresh cloth, etc. 
- Managers who take initial orders, receive complaints, prepare and hand out the bills, etc.
- Servers who serve the food on the table.
- Executive chef who receives orders and expedites food preparation
- Line cooks who prepare the food.
- Order register where the customer orders are stored
- Cash register where bills are generated and payments are stored
- Feedback system where customer complaints and feedback comments are stored
- The general restaurant facilities such as tables, chairs, utensils, so on so forth.

# General Restaurent Management Syetem
- Customer walks in
- Host allots a table for him / her
- Bus boy serves water
- Manager shows the menu to the customer
- Customer orders food
- Manager places order
- Executive chef gets order and instructs other line cooks
- Once food is prepared, the server serves food
- On completion, customer requests a bill from the manager
- Customer gives feedback, pays bill and leaves
- Payment is registered in the cash registered
- Bus boy cleans up table

# Technical requirements
- Typescript is the programming language and Angular is the webframework to be used to develop the solution
- Angular Material themeing must be implemented
- npm should be used as the build system for compiling and producing the final js code
- Wherever possible, decouple data from the system. e.g. the menu items could be read from a file rather than hard coding within the solution

# Software requirements
  -Node.js, npm
  -github scm
  -visual studio code
  -need install the package **npm** into project folder before running the project folder

# Starting Project
  There are many ways to start the the **angular js** project. This project was based on the quick start at Angular Github.
  By cloning quick star at git we can get the necessary code to get started.
   To clone the quickstart we can use **github url - https://github.com/angular/quickstart projectname(maheshkumar)** 
  
# Running the Project
  starting the project
  After the project code completed we need to run the code. to run the code we have go to the project folder in the command prompt and install the **npm**.
  Installling the **npm**
  
  C:\projectname(maheshkumar)>npm install
  
  After install we run project using command **ng serve**
  Running the project
  C:\projectname> **$ ng serve**
  
  This will start the localhost with port number 4200 in our machine.
  Go to the browser and type the **Localhost:4200"**.
  
  By above process we run our project.

  References : 
  https://www.tutorialspoint.com/angular2/angular2_hello_world.html
  https://github.com/sachin26049
  
  